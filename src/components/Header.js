import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { IconButton, Colors } from 'react-native-paper';

const Header = () => {
    return (
        <View style={styles.headerContainer}>
            <Text style={styles.headerTitle}>PAA</Text>
            <IconButton
                animated={true}
                icon='account-circle'
                color={Colors.white}
                size={24}
                onPress={() => console.log('account pressed')}
                style={{marginHorizontal: 16}}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: '#4696ec',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: Platform.OS === 'ios' ? 44 : 0
    },
    headerTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: '700',
        fontStyle: 'italic',
        // paddingVertical: 16,
        marginHorizontal: 16,
        textAlignVertical: 'center'
        // textAlign: 'center'
    }
});

export default Header;
