import React, { useEffect } from 'react'
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native'

const GRID_MARGIN = 0.8
const GRID_BORDER_RADIUS = 8

const ImageGrid = ({ images, size, parentMargin }) => {
    const totalRows = Math.ceil(images.length / size)

    const chunks = (arr, size) => {
        return Array.from(
            new Array(Math.ceil(arr.length / size)),
            (_, i) => arr.slice(i * size, i * size + size)
        )
    }
    
    const calculatedSize = (actualSize, size, totalRows) => {
        let dividedWidth = (Dimensions.get('window').width - parentMargin - (GRID_MARGIN * (2 + size - 1))) / size
        let height = (Dimensions.get('window').width - parentMargin - (GRID_MARGIN * (2 + actualSize - 1))) / actualSize

        return {
            width: dividedWidth,
            height: totalRows > 2 ? height : dividedWidth
        }
    }

    const calculatedBorderRadius = (rowSize, colSize, row, col) => {
        let borderRadiiStyle = {
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            borderBottomLeftRadius: 0,
            borderBottomRightRadius: 0
        }

        if (row === 0 && col === 0) {
            borderRadiiStyle.borderTopLeftRadius = GRID_BORDER_RADIUS
        }
        if (row === 0 && col === colSize - 1) {
            borderRadiiStyle.borderTopRightRadius = GRID_BORDER_RADIUS
        }
        if (row === rowSize - 1 && col === 0) {
            borderRadiiStyle.borderBottomLeftRadius = GRID_BORDER_RADIUS
        }
        if (row === rowSize - 1 && col === colSize - 1) {
            borderRadiiStyle.borderBottomRightRadius = GRID_BORDER_RADIUS
        }

        return borderRadiiStyle
    }

    const calculatedMargin = (col) => {
        if (col === 0) {
            return { marginLeft: 0 }
        } else {
            return { marginLeft: GRID_MARGIN }
        }
    }

    const calculatedMask = (moreThanSizeSquared, row, col) => {
        if (moreThanSizeSquared && row == size - 1 && col === size - 1) {
            return true
        } else {
            return false
        }
    }

    const renderRow = (imagesForRow, row, moreThanSizeSquared, extra) => {
        if (row < size) {
            return imagesForRow.map((uri, col) => {
                return (
                    <View>
                        <Image
                            key={row + ':' + col}
                            source={{ uri: uri }}
                            style={[styles.item, calculatedSize(size, imagesForRow.length, Math.ceil(images.length / size)), calculatedBorderRadius(Math.min(size, Math.ceil(images.length / size)), imagesForRow.length, row, col), calculatedMargin(col)]}
                        />
                        {calculatedMask(moreThanSizeSquared, row, col) && (
                            <View
                                key={row + ':' + col + 'view'}
                                style={[calculatedSize(size, imagesForRow.length, Math.ceil(images.length / size)), calculatedMargin(col), {position: 'absolute', backgroundColor: 'rgba(255, 255, 255, 0.5)'}]}
                            >
                                <Text
                                    key={row + ':' + col + 'text'}
                                    style={[calculatedSize(size, imagesForRow.length, Math.ceil(images.length / size)), {fontSize: 48, fontWeight: '600', textAlign: 'center', textAlignVertical: 'center', color: '#333'}]}>
                                    {'+' + extra}
                                </Text>
                            </View>
                        )}
                    </View>
                )
            })
        }
    }

    return chunks(images, size).map((imagesForRow, i) => {
        return (
            <View
                key={i}
                style={styles.imageRowContainer}
            >
                {renderRow(imagesForRow, i, images.length > size * size, images.length - size * size)}
            </View>
        )
    })
}

const styles = StyleSheet.create({
    imageRowContainer: {
        flexDirection: 'row',
        borderColor: 'white',
        borderTopWidth: GRID_MARGIN / 2,
        borderBottomWidth: GRID_MARGIN / 2,
        borderStartWidth: GRID_MARGIN,
        borderEndWidth: GRID_MARGIN
    },
    item: {
    }
})

export default ImageGrid
