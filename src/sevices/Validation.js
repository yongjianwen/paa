
export const isNotNullAndEmpty = (value) => {
    return value !== undefined && value !== null && value !== ''
}
