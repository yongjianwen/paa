import React, { useEffect, useState } from 'react';
import {
    FlatList,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { Avatar, Button, Card, Chip, Title, Paragraph } from 'react-native-paper';
import Header from '../../components/Header';
import ImageGrid from '../../components/ImageGrid'
import { isNotNullAndEmpty } from '../../sevices/Validation';
import Icon from 'react-native-vector-icons/FontAwesome'

const Listing = ({ navigation }) => {
    console.log(navigation)
    // const navigation = useNavigation()

    let shelterNames = ['SOSD Singapore', 'Kitten Sanctuary Singapore', 'The Animal Lodge', 'Action For Singapore Dogs', 'Causes For Animals']


    const [pets, setPets] = useState([]);

    useEffect(() => {
        setPets([
            {
                shelterName: 'SOSD Singapore',
                shelterPicture: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANsAAADmCAMAAABruQABAAAAyVBMVEX///8jHyCtFysAAACrAB+oAAvt2drIfYOmAAALAAOop6fV1NW/vr4JAAC7u7v09PQdGBns7OwWERIbFhdMSkuqABmrACASCw14dneoABCsDiaXlpe0tLRiYGGoAAr39/fe3t4zMDGqqqqNjIy0PEjy4+RrampXVVbmyMqxLDvQkpfYp6vDbnXguby5T1n37e49OjtRT0/HeYC/YWnZqq3kw8bUnaKRkJA0MTHpz9HMy8tEQkIoJSauIDHNi5C7VV63RlCzNULAZ252him9AAAJxUlEQVR4nO2dbV/aShOHgcUKISEhCAFBg9KCrbZqj60KtLXn+3+oO9nZJLubx9Mb2E1/c70xhvV0/uxDZmdmcxoNBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQpAbcfPp6+v2daisOwe3oZNRujzofb1Rbsm/899MWcN75qtqY/eK3xq2Y6T+qzdkrb5y0QNwn1fbskVsYkKPOZZtedD6otmhv+CfQXU/3H047dM59UW3S3jhtJ511S3V2vqm2aU98oH01/Q6//ToPO66l1qS98YWq+cF+A6Wj70pN2hffqJhO/Mg+HdHfbZU27YsW7bZfyQ2qbXynzqK98X0kr/pP9InQqb9neQ/T65S/95v25HtVJu2NO+qRjIR7r1TvyWdFJu2Ld7D+P4l3/6GC22pM2hvv6fD7KN1lA/WnEpP2xWfwQl7l+59ggblXYdO+oM7WOGNPc573QW34mds9ny+zO7Q2sGmVuVvLnoj1AZbD88zPshfQ2gCPscucx9gdnYvT45q0Nz4Wuh8+Vd4+zflYb8rcxrSjWRt8Kq1d4O7/OK9reAECCZd+fgu2satfeCHZXtvLeW/QaCwuLmY9sc2XmoYXkkCCSSxC3DVxhx5xlwuuTU3DC9x4s0kzwGlSPGJwrVgArGDc6kiLWyeWpMlB+EBJ6XqjIeL6/uzx2kyuXQ3DCzb/XO5PZi+cNm/Gtyx+vuuI4E/5hO+2ZtPlWxb7ZRoi+cET4ggTbs63rVt4Qd6/bHb8auJN+LZF+yANyQgkGMRNxuRKaF2v8EJWIMFfxeKGa7H5uEbhheyOGMTD0tmKn+TFizQkbwJxU076pD7hBVj4xqn7F8N4oZQ+qU14IfeBlUw4MpA+qkt4Ic/RsJMxSTbyZ/UIL+Q6iGfxkJS8rpB6hBfyHPuJled1UVo1CC/kbcgWwjZH9ExCahBeyN1IP4vucmrGSfl+Hcm18EHwlgPfRG4g1mloSO7I8okoremt5Ca6hxdyV4SerE3a6DTicKau9Yf5K3laW/oBfqtzeKEgwJ8ak8GM28qN4Kn/dnhD/4Aiz+nKSXecIbXROLxQ6PEuvZQ2MZgXkudlq+etaKeSMeGarrRH1Te8UJLATo9JKVIZkp8eV0tJZGCWMSibntwqt6xBKWURnUHGoEz7lVqGF8oLfSZuhjiykFpBeOH3IU39z5RHUDM7znmQWmkYXqhQWJepLf2Q+3esW3ihQsbi2srSlnrIweBu61PFViXTtKIxhdRq6V5I7XQLL1BpJQXIdEi6s26q4+Rtaqq8WSls61VYOE6DCs5ZI71aOnGTBf3xDZwAPU6SVcrIG2GHebNGP7WkeEvWZMCeCG8ahRcqHdSgkddwz7ZKTbl4J7fe0R8svHB7WKsrcVMpRBV22/AxvNrK4uJwpckczK/ahBcqVS7R6Ub69HolDcs4r+MT8MHg2JUG4YVqa/Y81GOxX/pijjjOffiEqdQkvFCxUjDcm8aLRsN+GQra2IQzSeSC6RFegLkxLZsbu6YY/dk5aW2+E0f2bnQIL7yrsqbNJxeulAoWtnPec3jLHDrJcxzW3uxq4GNRGEhg9Iae50i7bHHHQ1bG844EP+LPNQgvVKqEXxJv9ehEYfLF5HE5X0qOs9sN1BM+1qw+vFCpxMDvNRpnDnuKGWGpoeU1u8SKp1x44ZJhX/grOCT376EsL6VyaUiPREFywyWkaxHPaNgem3PeCyHWSo4Lsb2FqvBC5RND/jZYAqOl1DYNA9aMJZ10oReZ5Wb/Vlq9AIHk8lKsCXGcl9TdwYYulnK1ScyryvDCfcUTeoE0T8ps2JMHQqA8j/Ry/gzCC4o67mlUGkig9B/PluKgW5BgWXRISDeVQY2B707NDpxuSad/socMHEqXPJoDe5Hback/oMQ5ob7Dn+1EjIdVoSrgZlq+5z0Q1CfpHPAfeFWnjZ4Anh6wbELhmLwF1+Fgu6ynS3VrCaxjrc77r6cH4O4HdUxU5QZg79Y6H7cPwPi8pTSWx/79AzJS5izft8fl5v0/TN9USQuc4F+dA6prd9SmPF7vTjqXJwfgsnP+U33i+/7dQVAvDEEQBEGQOtKbGyHzCkGQkIEJzTdJsMUf2DEHMvKPmIWxuJDg57K8+fyKECtsbhFyEcUtDcKxm+RH9o6Kf8UnZbpO2de+4jPCbpRyM/iiGsclL4tD212FM7EcZpiOjgvMpEw+izgbUsGQkyo/VMBGLoZJnWoQSFXTu1CbIGsL+u4o5hfCjiMOg2nCruQSa4E5aHMsEp3WhyoFps0ZDqMRa8nlh8cHTHHXfdNcdTljc4BksLObm+YzyIRkB2hzztbrLROdkfo5NvByEiieWNPSilTdPw9rQicZ6IE8OLtehNcsY1z43zkGcIyUlY4wA+WTNjxntJ+hcAsO/Flz7k97XKP87NWRELRBbWu5NueKXudqu+Y6VCFF2uzZekdzbNtl3AWVtJn0AyIm949PgTaDdFlpk+ORR+Ze/R3axIe0B3L+Dm1yzTyB+pK/Qlt0MtiJHsZd2qTSOqmJNp9/vhnEDQBtIGLordcv7Bm9CG8/OsmjawHNtdXGXt1BrqlNq0nAihpOu9A5Cy/5ZzQcE4BXzwyg+YZro5e2LQw58izdh/HWT67BVFZc2JWdTi21zdjujWxFF4kkfQV9CKZGBxhdT/Q6tNQWv09mKPoROdoa62i7J/a0ntqSDRzhj+rlabO70SZG6Gk9tQXiImu7V4krmaetMYgPHPE9ram2xuAq2jQ7yaY7Vxt/QCDpaV21haV2KWsLtAWb77inH5ifqa+2wJqo4j+K4hVpawxeop522ZEAjbU17G3UdSwqV6iN62kLlkudtQUOJLOWxYJKtCU9zceCdNXW6HV5Y8u0BT3tcv2sozYbotw03jbgneIcbQ+0OXQtvGjHoq6ontoS+6LCcWpgjja6PRjCAVqQAAcHtNc2t/6TtgFqUwZqQ22o7XgcRJtWMfMu5Mr2pU2TXAdoYMau4kNf7D7YbXNDDHJUcBoavgr4XnTMUbGXP3RXpmlOeJvoZdOaLxYb1g3Ub2S5xYe5abKgOp9/0yy3yHLCbpwTdnb8fSu473CS2Sta+Jwwl2dsOgFN9rWozwmbcnLegsC//MILeJkC/0pbAMKzGbn81AuuFPDgZtskHE9PVoZnSRy7r2UNRsNu8mZ5sU0D/n8b4JD43bwXfO3MEILtsjZdamfCA9xQwxQWMU2Sxc2eRPcJeeGKmOZNViIVNH9csJtCzVNTl5qnkN7cmM1mxlwyyd9cz8L7fSkDvuiHzWfXm+SL8G09a9UQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEGOzv8APYHVQ69geKcAAAAASUVORK5CYII=',
                postDate: '23 March 2022 10:45 PM',
                name: 'Birdie "Bluka" for your adoption',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                image: 'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                images: [
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg',
                    'https://cdn.cnn.com/cnnnext/dam/assets/201030094143-stock-rhodesian-ridgeback-super-tease.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg'
                ]
                // image: 'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg'
            },
            {
                shelterName: 'Kitten Sanctuary Singapore',
                shelterPicture: 'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg',
                name: 'Dog Name 2',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=1.00xw:0.669xh;0,0.190xh&resize=1200:*',
                images: [
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg'
                ]
            },
            {
                shelterName: 'The Animal Lodge',
                shelterPicture: 'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg',
                name: 'Dog Name 3',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                image: 'https://cdn.cnn.com/cnnnext/dam/assets/201030094143-stock-rhodesian-ridgeback-super-tease.jpg',
                images: [
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg'
                ]
            },
            {
                shelterName: 'Action For Singapore Dogs',
                shelterPicture: 'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg',
                name: 'Dog Name 4',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                image: 'https://www.edinburghnews.scotsman.com/webimg/b25lY21zOmFmM2U5NGMyLTMxNDgtNGFhMS05MmRlLTQwNjc2NGM0Mjg0YTo5ZDc2NzUxNS02MGY5LTQ0ZTgtOTUwNi0xMDgzZThkMTc1MmE=.jpg?&width=990',
                images: [
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg',
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg'
                ]
            },
            {
                shelterName: 'Causes For Animals',
                shelterPicture: 'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg',
                name: 'Dog Name 4',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                image: 'https://www.edinburghnews.scotsman.com/webimg/b25lY21zOmFmM2U5NGMyLTMxNDgtNGFhMS05MmRlLTQwNjc2NGM0Mjg0YTo5ZDc2NzUxNS02MGY5LTQ0ZTgtOTUwNi0xMDgzZThkMTc1MmE=.jpg?&width=990',
                images: [
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg'
                ]
            },
            {
                shelterName: 'Causes For Animals',
                shelterPicture: 'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg',
                name: 'Dog Name 4',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                image: 'https://www.edinburghnews.scotsman.com/webimg/b25lY21zOmFmM2U5NGMyLTMxNDgtNGFhMS05MmRlLTQwNjc2NGM0Mjg0YTo5ZDc2NzUxNS02MGY5LTQ0ZTgtOTUwNi0xMDgzZThkMTc1MmE=.jpg?&width=990',
                images: [
                ]
            },
            {
                shelterName: 'Causes For Animals',
                shelterPicture: 'https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg',
                name: 'Dog Name 4',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                image: 'https://www.edinburghnews.scotsman.com/webimg/b25lY21zOmFmM2U5NGMyLTMxNDgtNGFhMS05MmRlLTQwNjc2NGM0Mjg0YTo5ZDc2NzUxNS02MGY5LTQ0ZTgtOTUwNi0xMDgzZThkMTc1MmE=.jpg?&width=990',
                images: [
                    'https://www.thesprucepets.com/thmb/V_YLRlAIKfTutBEzmSYwyEJP7OU=/1975x1518/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-597187685-58ad8e7a3df78c345b864b4f.jpg'
                ]
            }
        ]);
    }, []);

    const truncate = (str) => {
        return str.substring(0, 200) + '...'
    }

    const renderItem = ({ item, index }) => {
        const leftContent = () =>
            <Avatar.Image
                size={38}
                source={{ uri: item.shelterPicture }}
                style={{backgroundColor: 'white'}}
            />

        return (
            <View style={styles.listingView}>
                <Card
                    onPress={() => {
                        console.log('press: ' + item.name + ' at index: ' + index)
                        navigation.navigate('ListingDetail')
                    }}
                    onLongPress={() => { console.log('long press: ' + item.name + ' at index: ' + index); }}
                >
                    <Card.Title
                        title={item.shelterName}
                        subtitle={item.postDate}
                        left={leftContent}
                    />
                    <Card.Content>
                        <Title>{item.name}</Title>
                        <Paragraph>{truncate(item.desc)}</Paragraph>
                        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                            <Chip icon='dog' onPress={() => console.log('Pressed')} style={styles.chipView}>Bird</Chip>
                            <Chip onPress={() => console.log('Pressed')} style={styles.chipView}>Test Breed</Chip>
                            <Chip icon='check-decagram' onPress={() => console.log('Pressed')} style={styles.chipView}>HDB Approved</Chip>
                        </View>
                    </Card.Content>
                    {isNotNullAndEmpty(item.images) &&
                        <View style={{marginTop: 8}}>
                            <ImageGrid
                                images={item.images}
                                size={3}
                                parentMargin={30}
                            />
                        </View>
                    }
                    {/* <Image
                        source={{
                            uri: item.image,
                            width: '100%',
                            height: 180
                        }}
                    /> */}
                    {/* <Card.Cover source={{ uri: 'https://picsum.photos/700' }} /> */}
                    <Card.Actions>
                        <View style={{flexDirection: 'row'}}>
                            <Button compact={true} icon='share-variant-outline' onPress={() => { console.log('shared') }} style={styles.listingButton} ></Button>
                            <Button compact={true} icon='comment-processing-outline' onPress={() => { console.log('commented') }} style={styles.listingButton}></Button>
                            <Button compact={true} icon='thumb-up-outline' onPress={() => { console.log('liked') }} style={styles.listingButton}></Button>
                        </View>
                    </Card.Actions>
                </Card>
            </View>
        );
    };

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
            <Header />
            <View style={{flex: 1}}>
                <FlatList
                    contentContainerStyle={{ paddingBottom: 15 }}
                    data={pets}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderItem}
                    style={{paddingTop: 7.5}}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    listingView: {
        backgroundColor: '#fff',
        minHeight: 280,
        marginHorizontal: 15,
        marginVertical: 7.5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 0.1,
        elevation: 5,
        borderRadius: 8,
        padding: 0,
        overflow: 'hidden'
    },
    profilePicture: {
        backgroundColor: '#fff',
        width: 64,
        height: 64,
        borderRadius: 100,
        overflow: 'hidden',
        margin: 10
    },
    listingTitle: {
        fontSize: 38,
        fontWeight: '500',
        color: '#000'
    },
    listingButton: {
        flex: 1,
        // borderWidth: 2,
        // borderColor: 'black',
        fontSize: 10
    },
    chipView: {
        marginTop: 4,
        marginEnd: 2,
        // padding: 0,
        // flex: 1,
        // height: 24,
        justifyContent: 'center'
    }
});

export default Listing;
