import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import Header from '../../components/Header';
import BaseView from '../../components/BaseView';

const Login = () => {
    return (
        <View>
            <Header />
            <View style={styles.wholePageNoScroll}>
                <Text style={styles.importantText}>This page is under construction</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    wholePageNoScroll: {
        width: '100%',
        height: '100%',
        backgroundColor: '#c0ccb6',
        flexDirection: 'row'
    },
    importantText: {
        width: '100%',
        fontSize: 45,
        fontWeight: '500',
        color: '#000000',
        textAlign: 'center',
        alignSelf: 'flex-start'
    }
});

export default Login;
