import React from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';
import Home from './src/Home';
import Login from './src/pages/login/Login';
import Listing from './src/pages/listing/Listing';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { COLORS } from './src/constant/Colors';
// import { NavigationContainer } from '@react-navigation/native'
import AppContainer from './src/navigation/AppNavigation'
// import { createStackNavigator } from '@react-navigation/stack'

// const Stack = createStackNavigator()

export default function App() {
  return (
    // <NavigationContainer>
      <PaperProvider
        theme={theme}
      >
        <View style={styles.container}>
          <StatusBar
            backgroundColor={'transparent'}
            translucent={true}
          />
          <Listing />
          {/* <AppContainer /> */}
        </View>
      </PaperProvider>
    // </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.primaryColor,
    flex: 1,
    paddingTop: StatusBar.currentHeight
  },
});

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: COLORS.primaryColor,
    accent: 'yellow',
  },
};
